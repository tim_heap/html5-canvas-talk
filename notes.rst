========================
HTML <canvas> talk notes
========================

#1 - Title
==

Welcome!

#2 - Me
==

Babble about myself.

#3 - <canvas>
==

The canvas element.
Regular element like all others.
inline-block by default.
Can have all properties by default.
Can contain child elements, will be displayed as a fallback if canvas not supported.
Transparent. Can be stacked with other images/canvas/elements.

#4 - Getting the canvas reference
==

Canvas may not be supported, check return values!
Modernizr can test for canvas support!
'2d' parameter leaves future scope for '3d', etc.

#5 - Basic drawing
==

Live demo!
Nothing interesting, just talk about what is happening on screen.
Move on.

#6 - Polygons
==

Make note of: beginPath, moveTo/lineTo, closePath.
Using both fill and stroke.
Play around with the live editor.

#7 - Images
==

Draw the image multiple times.
Note scaling, streting, etc.
Draw multiple images? `img/spinning.gif`.
Note lack of animated gif.

#8 - Gradients
==

Gradient: linear and radial.
Must set origin and destination.
Colour stops set in percentage of distance.
Can be used as fill and stroke styles.
Can be transparent.

#9 - Animate notes
==

Nothing much interesting, move on to the demo.

#10 - Animate demo
===

animate is a helper function.
clearRect is important!
All is done manually, no interpolation helpers, timing done manually, etc.

#11 - Context
===

Summary of context.
Brief talk on what has been covered.
Brief talk on what has not been covered.
Intro to state and transformations.

#12 - Transformations
===

Cover each of the types of transformations.
Dont linger, as the demos cover most of it.
Matrix math is useful, but not required.
Matrix operations happen in sequence, are not commutable.

#13 - Translation
===

Translation is useful for both animation *and* resetting the origin to something useful.
Stack translations: One to centre the drawing, then one to rotate it around.
Add some extra shapes around the square.

#14 - Scale
===

Not just for stretching squishing, also for resetting scale for more useful drawing operations later.
Note that stroke is stretched/squashed with scale, despite the number being the same.

#15 - Rotation
===

Only way to spin some things.
Spins around (0, 0). Note translation before rotation.
Try resetting translation to (0, 0), image top-left to (0, 0).

#16 - Matrix
============

Matrix operations.
Dont go in to detail. If you know matrices, they are useful. If not, dont worry.
Play with the numbers a little bit.

#17 - Orbit demo
================

Go look at the code!
