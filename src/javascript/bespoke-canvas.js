var animateFactory = require('./animate');
var td = require('throttle-debounce');
var $ = require('jquery');

var ace = require('brace');
require('brace/mode/javascript');
require('brace/theme/monokai');

var createExample = function(deck, slide) {
	var code = slide.querySelector('[data-example-code]');
	var canvas = slide.querySelector('canvas[data-example-demo]');
	var error = slide.querySelector('[data-example-error]');

	var oldCode = null;
	var animate = animateFactory();

	var $code = $(code);
	$code.css({'width': $code.outerWidth(), 'height': $code.outerHeight()});

	var editor = ace.edit(code);
	editor.getSession().setMode('ace/mode/javascript');
	editor.setTheme('ace/theme/monokai');
	editor.setFontSize(24);

	var runCode = function() {
		try {
			// Make the function. This may throw a SyntaxError
			var fn = new Function('canvas', 'context', 'animate', editor.getValue());
			// Clear the canvas and get a fresh context
			canvas.width = canvas.width;
			var context = canvas.getContext('2d');

			// Stop any previous animation before starting a new one
			animate.stop();

			// Any errors in the code will throw from here
			fn(canvas, context, animate);
		} catch (e) {
			console.error(e);
		}
	};

	var runIfDifferent = function() {
		var newCode = code.value;
		if (newCode === oldCode) return;
		oldCode = newCode;

		runCode();
	};

	var session = editor.getSession();
	editor.commands.addCommand({
		name: 'run',
		bindKey: {win: "Ctrl-Return"},
		exec: runCode,
	});

	deck.on('activate', function(e) {
		if (e.slide != slide) return
		runCode();
	});

	deck.on('deactivate', function(e) {
		if (e.slide == slide) return;
		animate.stop();
	});
};

module.exports = function() {
	return function(deck) {
		deck.slides.forEach(function(slide) {
			if (slide.dataset.hasOwnProperty('example')) {
				createExample(deck, slide);
			}
		});
	};
};
