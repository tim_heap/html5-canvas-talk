module.exports = function animate() {
	var current = null;
	var animate = function animate(fn) {
		animate.stop();
		var callback = function(now) {
			fn(now);
			current = window.requestAnimationFrame(callback);
		};
		current = window.requestAnimationFrame(callback);
	};
	animate.stop = function() {
		if (current) {
			window.cancelAnimationFrame(current);
			current = null;
		}
	};
	return animate;
};
