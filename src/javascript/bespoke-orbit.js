module.exports = function() {
	return function(deck) {
		deck.slides.forEach(function(slide) {
			if (slide.dataset.hasOwnProperty('orbit')) {
				new OrbitDemo(deck, slide);
			}
		});
	}
};

var inContext = function(fn) {
	return function(context) {
		context.save();
		fn.apply(this, arguments);
		context.restore();
	};
};

var simpleCircle = function(color, size) {
	return function(context, time) {
		context.beginPath();
		context.fillStyle = color;
		context.arc(0, 0, size, 0, 2 * Math.PI);
		context.fill();
	};
};


var OrbitDemo = function(deck, slide) {
	var self = this;
	this.deck = deck;
	this.slide = slide;
	this.canvas = slide.querySelector('canvas[data-orbit-demo]')
	this.context = this.canvas.getContext('2d');

	this.centre = new Body('Sol', simpleCircle('#ffff00', 10), [
		new Satellite(20, 20, new Body('Mercury', simpleCircle('#aaaaaa', 3))),
		new Satellite(30, 15, new Body('Venus', simpleCircle('#ffaa33', 4))),
		new Satellite(50, 11, new Body('Earth', simpleCircle('#00ff00', 5), [
			new Satellite(10, 17, new Body('Luna', simpleCircle('#aaaaaa', 2))),
		])),
		new Satellite(80, 7, new Body('Mars', simpleCircle('#ff0000', 3), [
			new Satellite(8, 23, new Body('Phobos', simpleCircle('#cc3333', 1))),
			new Satellite(10, 29, new Body('Diemos', simpleCircle('#cc3333', 1))),
		])),
	]);

	deck.on('activate', function(e) {
		if (e.slide != slide) return;
		self.start();
	});

	deck.on('deactivate', function(e) {
		if (e.slide != slide) return;
		self.stop();
	});
};

OrbitDemo.prototype = Object.create(null);
OrbitDemo.prototype.render = inContext(function(context, time) {
	context.clearRect(0, 0, this.canvas.width, this.canvas.height);

	var scaleFactor = (Math.min(this.canvas.width, this.canvas.height)) / (2 * 100);
	this.context.translate(this.canvas.width * 0.5, this.canvas.height * 0.5);
	this.context.scale(scaleFactor, scaleFactor);

	this.centre.render(context, time);
});
OrbitDemo.prototype.start = function() {
	this.stop();
	var self = this;
	var enqueue = function() {
		self.timer = window.requestAnimationFrame(loop);
	};
	var loop = function(time) {
		if (self.timer === null) return;
		self.render(self.context, time);
		enqueue();
	}
	enqueue();
};
OrbitDemo.prototype.stop = function() {
	if (this.timer) {
		window.cancelAnimationFrame(this.timer);
	}
	this.timer = null;
};

var Body = function(name, render, satellites) {
	this.name = name;
	this.renderBody = inContext(render);
	this.satellites = [];
	if (satellites) {
		satellites.forEach(function(satellite) {
			this.addSatellite(satellite);
		}, this);
	}
};

Body.prototype = Object.create(null);
Body.prototype.addSatellite = function(satellite) {
	this.satellites.push(satellite);
};
Body.prototype.render = inContext(function(context, time) {
	this.renderBody(context, time);
	this.satellites.forEach(function(satellite) {
		satellite.render(context, time);
	}, this);
});

var Satellite = function(distance, velocity, child) {
	this.distance = distance;
	this.velocity = velocity;
	this.child = child;
};

Satellite.prototype = Object.create(null);
Satellite.prototype.render = inContext(function(context, time) {
	this.renderOrbit(context, time);

	var theta = time * this.velocity / 10000;
	context.translate(
		Math.cos(theta) * this.distance,
		Math.sin(theta) * this.distance);
	this.child.render(context, time);
});
Satellite.prototype.renderOrbit = inContext(function(context, time) {
	context.beginPath();
	context.strokeStyle = '#888888';
	context.setLineDash([2, 5]);
	context.lineDashOffset = -time / 100;
	context.lineWidth = 0.5;
	context.arc(0, 0, this.distance, 0, 2 * Math.PI);
	context.stroke();
});
